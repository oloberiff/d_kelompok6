<?php

namespace App\Http\Controllers;
use App\Occurrence;
use Illuminate\Http\Request;

class OccurrenceController extends Controller
{
    public function index()
    {
        $occurrence = \App\Occurrence::paginate();
        return view('occurrence.index',compact('occurrence'));
    }

    public function create()
    {
        $occurrence = Occurrence::all();
        return view('occurrence.create', ['occurrence'=>$occurrence]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'date'=>'required|date',
            'description'=>'required',
            'picture'=>'required|image|mimes:jpg,jpeg,png,bmp',
        ]);

        $file = $request->file('picture');
        $path = $file->store('picture', 'public');

        Occurrence::create([
            'name'=>$request->name,
            'date'=>$request->date,
            'description'=>$request->description,
            'picture'=>$path,
        ]);

        return redirect()->route('occurrence.create')->with('status', 'Sky Event Data was Successfully Created');
    }

    public function show($id)
    {
        $occurrence = Occurrence::findOrFail($id);
        return view('occurrence.show',['occurrence'=>$occurrence]);
    }

    public function edit($id)
    {
        $occurrence = Occurrence::findOrFail($id);
        return view('occurrence.edit',['occurrence'=>$occurrence]);
    }

    public function update(Request $request, $id)
    {
        $update_spacecraft = Occurrence::findOrFail($id);
        $request->validate([
            'name'=>'required',
            'date'=>'required|date',
            'description'=>'required',
            'picture'=>'required|image|mimes:jpg,jpeg,png,bmp',
        ]);

        $update_spacecraft->update([
            'name'=>$request->name,
            'date'=>$request->date,
            'description'=>$request->description,
            'picture'=>$request->picture,
        ]);

        if($request->file('picture')) 
        {
            if ($update_spacecraft->picture && file_exists(storage_path('app/public/'.$update_spacecraft->picture))) 
            {
                Storage::delete('public/'.$update_spacecraft->picture);
            }

            $file = $request->file('picture')->store('picture', 'public');
            $update_spacecraft->picture = $file;
        }

        $update_spacecraft->save();
        return redirect()->route('occurrence.edit',['occurrence'=>$update_spacecraft])->with('status', 'Sky Event Data was Updated Successfully');
    }

    public function destroy($id)
    {
        $occurrence = Occurrence::findOrFail($id);
        if ($occurrence->picture && file_exists(storage_path('app/public/'.$occurrence->picture))) 
        {
            Storage::delete('public/'.$occurrence->picture);
        }
        $occurrence->delete();
        return redirect()->back()->with('status', 'Sky Event was Successfully deleted');
    }

    public function search(Request $request)
    {
       $cari = $request->get('search');
       $occurrence = Occurrence::where('name', 'LIKE', '%'.$cari.'%')->orderBy('created_at', 'DESC')->paginate(5);
       return view('occurrence.index', ['occurrence'=>$occurrence]);
    }
}
