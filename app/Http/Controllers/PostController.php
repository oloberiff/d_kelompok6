<?php

namespace App\Http\Controllers;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $post = \App\Post::paginate();
        return view('post.index',compact('post'));
    }

    public function create()
    {
        $post = Post::all();
        return view('post.create', ['post'=>$post]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required|min:5',
            'bodypost'=>'required|min:5',
            'picture'=>'required|image|mimes:jpg,jpeg,png,bmp',
        ]);

        $file = $request->file('picture');
        $path = $file->store('picture', 'public');

        Post::create([
            'title'=>$request->title,
            'bodypost'=>$request->bodypost,
            'picture'=>$path,
        ]);

        return redirect()->route('post.create')->with('status', 'Post was Successfully Created');
    }

    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('post.show',['post'=>$post]);
    }

    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('post.edit',['post'=>$post]);
    }

    public function update(Request $request, $id)
    {
        $update_post = Post::findOrFail($id);
        $request->validate([
            'title'=>'required|min:5',
            'bodypost'=>'required|min:5',
            'picture'=>'required|image|mimes:jpg,jpeg,png,bmp',
        ]);

        $update_post->update([
            'title'=>$request->title,
            'bodypost'=>$request->bodypost,
            'picture'=>$request->avatar,
        ]);

        if($request->file('picture')) 
        {
            if ($update_post->picture && file_exists(storage_path('app/public/'.$update_post->picture))) 
            {
                Storage::delete('public/'.$update_post->picture);
            }

            $file = $request->file('picture')->store('picture', 'public');
            $update_post->picture = $file;
        }

        $update_post->save();
        return redirect()->route('post.edit',['post'=>$update_post])->with('status', 'Post was Updated Successfully');
    }

    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        if ($post->picture && file_exists(storage_path('app/public/'.$post->picture))) 
        {
            Storage::delete('public/'.$post->picture);
        }
        $post->delete();
        return redirect()->back()->with('status', 'Post was Successfully deleted');
    }

    public function search(Request $request)
    {
       $cari = $request->get('search');
       $post = Post::where('title', 'LIKE', '%'.$cari.'%')->orderBy('created_at', 'DESC')->paginate(5);
       return view('post.index', ['post'=>$post]);
    }
}
