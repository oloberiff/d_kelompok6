<?php

namespace App\Http\Controllers;
use App\SkyObject;
use Illuminate\Http\Request;

class SkyObjectController extends Controller
{
    public function index()
    {
        $skyobject = \App\SkyObject::paginate();
        return view('skyobject.index',compact('skyobject'));
    }

    public function create()
    {
        $skyobject = SkyObject::all();
        return view('skyobject.create', ['skyobject'=>$skyobject]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'type'=>'required',
            'distance'=>'required',
            'picture'=>'required|image|mimes:jpg,jpeg,png,bmp',
        ]);

        $file = $request->file('picture');
        $path = $file->store('picture', 'public');

        SkyObject::create([
            'name'=>$request->name,
            'type'=>$request->type,
            'distance'=>$request->distance,
            'picture'=>$path,
        ]);

        return redirect()->route('skyobject.create')->with('status', 'Sky Object Data was Successfully Created');
    }

    public function show($id)
    {
        $skyobject = SkyObject::findOrFail($id);
        return view('skyobject.show',['skyobject'=>$skyobject]);
    }

    public function edit($id)
    {
        $skyobject = SkyObject::findOrFail($id);
        return view('skyobject.edit',['skyobject'=>$skyobject]);
    }

    public function update(Request $request, $id)
    {
        $update_spacecraft = SkyObject::findOrFail($id);
        $request->validate([
            'name'=>'required',
            'type'=>'required',
            'distance'=>'required',
            'picture'=>'required|image|mimes:jpg,jpeg,png,bmp',
        ]);

        $update_spacecraft->update([
            'name'=>$request->name,
            'type'=>$request->type,
            'distance'=>$request->distance,
            'picture'=>$request->picture,
        ]);

        if($request->file('picture')) 
        {
            if ($update_spacecraft->picture && file_exists(storage_path('app/public/'.$update_spacecraft->picture))) 
            {
                Storage::delete('public/'.$update_spacecraft->picture);
            }

            $file = $request->file('picture')->store('picture', 'public');
            $update_spacecraft->picture = $file;
        }

        $update_spacecraft->save();
        return redirect()->route('skyobject.edit',['skyobject'=>$update_spacecraft])->with('status', 'Sky Object Data was Updated Successfully');
    }

    public function destroy($id)
    {
        $skyobject = SkyObject::findOrFail($id);
        if ($skyobject->picture && file_exists(storage_path('app/public/'.$skyobject->picture))) 
        {
            Storage::delete('public/'.$skyobject->picture);
        }
        $skyobject->delete();
        return redirect()->back()->with('status', 'Sky Object was Successfully deleted');
    }

    public function search(Request $request)
    {
       $cari = $request->get('search');
       $skyobject = SkyObject::where('name', 'LIKE', '%'.$cari.'%')->orderBy('created_at', 'DESC')->paginate(5);
       return view('skyobject.index', ['skyobject'=>$skyobject]);
    }
}
