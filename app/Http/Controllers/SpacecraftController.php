<?php

namespace App\Http\Controllers;
use App\Spacecraft;
use Illuminate\Http\Request;

class SpacecraftController extends Controller
{
    public function index()
    {
        $spacecraft = \App\Spacecraft::paginate();
        return view('spacecraft.index',compact('spacecraft'));
    }

    public function create()
    {
        $spacecraft = Spacecraft::all();
        return view('spacecraft.create', ['spacecraft'=>$spacecraft]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'manufacturer'=>'required',
            'launch_date'=>'required|date',
            'description'=>'required',
            'picture'=>'required|image|mimes:jpg,jpeg,png,bmp',
        ]);

        $file = $request->file('picture');
        $path = $file->store('picture', 'public');

        Spacecraft::create([
            'name'=>$request->name,
            'manufacturer'=>$request->manufacturer,
            'launch_date'=>$request->launch_date,
            'description'=>$request->description,
            'picture'=>$path,
        ]);

        return redirect()->route('spacecraft.create')->with('status', 'Spacecraft Data was Successfully Created');
    }

    public function show($id)
    {
        $spacecraft = Spacecraft::findOrFail($id);
        return view('spacecraft.show',['spacecraft'=>$spacecraft]);
    }

    public function edit($id)
    {
        $spacecraft = Spacecraft::findOrFail($id);
        return view('spacecraft.edit',['spacecraft'=>$spacecraft]);
    }

    public function update(Request $request, $id)
    {
        $update_spacecraft = Spacecraft::findOrFail($id);
        $request->validate([
            'name'=>'required',
            'manufacturer'=>'required',
            'launch_date'=>'required|date',
            'description'=>'required',
            'picture'=>'required|image|mimes:jpg,jpeg,png,bmp',
        ]);

        $update_spacecraft->update([
            'name'=>$request->name,
            'manufacturer'=>$request->manufacturer,
            'launch_date'=>$request->launch_date,
            'description'=>$request->description,
            'picture'=>$request->picture,
        ]);

        if($request->file('picture')) 
        {
            if ($update_spacecraft->picture && file_exists(storage_path('app/public/'.$update_spacecraft->picture))) 
            {
                Storage::delete('public/'.$update_spacecraft->picture);
            }

            $file = $request->file('picture')->store('picture', 'public');
            $update_spacecraft->picture = $file;
        }

        $update_spacecraft->save();
        return redirect()->route('spacecraft.edit',['spacecraft'=>$update_spacecraft])->with('status', 'Spacecraft Data was Updated Successfully');
    }

    public function destroy($id)
    {
        $spacecraft = Spacecraft::findOrFail($id);
        if ($spacecraft->picture && file_exists(storage_path('app/public/'.$spacecraft->picture))) 
        {
            Storage::delete('public/'.$spacecraft->picture);
        }
        $spacecraft->delete();
        return redirect()->back()->with('status', 'Spacecraft was Successfully deleted');
    }

    public function search(Request $request)
    {
       $cari = $request->get('search');
       $spacecraft = Spacecraft::where('name', 'LIKE', '%'.$cari.'%')->orderBy('created_at', 'DESC')->paginate(5);
       return view('spacecraft.index', ['spacecraft'=>$spacecraft]);
    }
}
