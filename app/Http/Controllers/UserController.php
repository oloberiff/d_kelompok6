<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = \App\User::paginate();
        return view('user.index',compact('user'));
    }

    public function create()
    {
        $user = User::all();
        return view('user.create', ['user'=>$user]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'username'=>'required|min:5|max:100|unique:users',
            'email'=>'required|email|unique:users',
            'avatar'=>'required|image|mimes:jpg,jpeg,png,bmp',
            'password'=>'required',
            'password_confirmation'=>'required|same:password'
        ]);

        $file = $request->file('avatar');
        $path = $file->store('avatars', 'public');

        User::create([
            'name'=>$request->name,
            'username'=>$request->username,
            'email'=>$request->email,
            'avatar'=>$path,
            'status'=>"ACTIVE",
            'password'=>$request->password,
            'password_confirmation'=>$request->password_confirmation
        ]);

        return redirect()->route('user.create')->with('status', 'User was Successfully Created');
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('user.show',['user'=>$user]);
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('user.edit',['user'=>$user]);
    }

    public function update(Request $request, $id)
    {
        $update_user = User::findOrFail($id);
        $request->validate([
            'status'=>'required',
            'avatar'=>'required|image|mimes:jpg,jpeg,png,bmp',
            
        ]);
            
        $update_user->update([
            'status'=>$request->status,
            'avatar'=>$request->avatar,
        ]);

        if($request->file('avatar')) 
        {
            if ($update_user->avatar && file_exists(storage_path('app/public/'.$update_user->avatar))) 
            {
                Storage::delete('public/'.$update_user->avatar);
            }

            $file = $request->file('avatar')->store('avatars', 'public');
            $update_user->avatar = $file;
        }

        $update_user->save();
        return redirect()->route('user.edit',['user'=>$update_user])->with('status', 'User was Updated Successfully');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user->avatar && file_exists(storage_path('app/public/'.$user->avatar))) 
        {
            Storage::delete('public/'.$user->avatar);
        }
        $user->delete();
        return redirect()->back()->with('status', 'User was Successfully deleted');
    }

    public function search(Request $request)
    {
       $cari = $request->get('search');
       $users = User::where('username', 'LIKE', '%'.$cari.'%')->orderBy('created_at', 'DESC')->paginate(5);
       return view('user.index', ['users'=>$users]);
    }
}
