<?php

namespace App\Http\Controllers;
use App\Post;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index(){
        $post = Post::paginate();
        return view('welcome',compact('post'));
    }
}
