<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Occurrence extends Model
{
    public $table = 'occurrence';
    protected $fillable = [
        'name', 'date','description',
    ];
}
