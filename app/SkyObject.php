<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkyObject extends Model
{
    public $table = 'skyobject';
    protected $fillable = [
        'name', 'type', 'distance',
    ];
}
