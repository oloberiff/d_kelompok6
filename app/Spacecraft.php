<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spacecraft extends Model
{

    public $table = 'spacecraft';
    protected $fillable = [
        'name', 'manufacturer', 'date_launch','description',
    ];
}
