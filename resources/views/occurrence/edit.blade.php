@extends('templates.home')
@section('title')
    Edit Sky Event Data
@endsection
@section('content')
    <div class="container" >
        <h3>Edit Sky Event Data</h3>
        <hr>
        @if (session('status'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{ session('status') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
        @endif
        <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
            <div class="card-header bg-primary text-white">
                <h5>Edit Sky Event Data</h5>
            </div>
            <div class="card-body">
                <div class="container text-primary">
                    <form action="{{ route('occurrence.update',$occurrence['id']) }}" method="POST" class="form-group" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-3">
                                <label for="name" class="text-primary">Sky Event Name</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control {{$errors->first('name') ? "is-invalid": ""}}" name="name" id="name" 
                                value="{{ old('name') ? old('name') : $occurrence['name'] }}">
                                    <div class="invalid-feedback">
                                        {{$errors->first('name')}}
                                    </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="date" class="text-primary">Date</label>
                            </div>
                            <div class="col-md-8">
                                <input type="date" class="form-control {{$errors->first('date') ? "is-invalid": ""}}" name="date" id="date" 
                                value="{{ old('date') ? old('date') : $occurrence['date'] }}">
                                    <div class="invalid-feedback">
                                        {{$errors->first('date')}}
                                    </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="description" class="text-primary">Description</label>
                            </div>
                            <div class="col-md-8">
                                <textarea class="form-control {{$errors->first('description') ? "is-invalid": ""}}" 
                                    name="description" id="description" value="{{ old('description') ? old('description') : $occurrence['description'] }}"></textarea>
                                    <div class="invalid-feedback">
                                        {{$errors->first('description')}}
                                    </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                Picture
                            </div>
                        <div class="col-md-8">
                                <img src="{{asset('storage/'.$occurrence['picture'])}}" class="img-thumbnail" height="150px" width="150px" alt="">
                                    <div class="custom-file">
                                        <label for="picture" class="custom-file-label">Picture</label>
                                            <input type="file" class="custom-file-input {{$errors->first('picture') ? "is-invalid": ""}}" name="picture" id="picture">
                                                <div class="invalid-feedback">
                                                    {{$errors->first('picture')}}
                                                </div>
                                    </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-sm-4">
                                <button type="submit" class="btn btn-outline-primary" >Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection