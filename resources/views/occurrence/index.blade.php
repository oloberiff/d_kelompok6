@extends('templates.home')
@section('title')
 	Index Post
@endsection
@section('css')
<style>
	body{
		padding-top: 30px;
	}
	th, td {
		padding: 10px;
		text-align: center;
	}
	td a{
		margin: 3px;
		align-content: center;
		color: white;
	}
	td a:hover{
		text-decoration: none;
	}
	td button{
		margin-top: 5px;
		cursor: pointer;
	 }
</style>
@endsection
@section('content')
	<div class="container">
		<h3> Post Index</h3>
		<hr>
		@if (session('status'))
		<div class="alert alert-info alert-dismissible fade show" role="alert">
			<strong> {{ session('status') }} </strong>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif
		<div class="row">
			<div class="col-md-2">
				<a href=" {{ route('occurrence.create') }} " class="btn btn-outline-primary">
					<span data-feather="plus-square"></span> Add Sky Event<span class="sr-only">(current)</span>
				</a>
			</div>
			<div class="col-md-8">
				<form action=" {{ route('occurrence.search') }} " method="get">
					<div class="input-group custom-search-form">
						<input type="text" name="search" placeholder="Filter by Name" class="form-control">
						<span class="input-group-btn">&nbsp;
							<button class="btn btn-outline-dark" type="submit">Filter</button>
						</span>
					</div>
				</form>
			</div>
		</div>
		<br>
		<div class="tabel-responsive">
			<table class="table table-hover">
				<thead>
					<tr class="table-primary">
						<th scope="col">ID</th>
						<th scope="col">Name</th>
                        <th scope="col">Date</th>
						<th scope="col">Picture</th>
						<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
				@foreach ($occurrence as $happen)
					<tr>
						<th>{{ $happen['id'] }}</th>
                        <th>{{ $happen['name'] }}</th>
                        <th>{{ $happen['date'] }}</th>
						<td>
							<img src="{{ asset('storage/'.$happen['picture']) }}" alt="N/A" width="80px">
						</td>
						<td>
							
							<a href="{{ route('occurrence.show', ['id'=>$happen['id']]) }}" class="btn-sm btn-primary">
								<span data-feather="eye"></span> Detail<span class="sr-only">(current)</span>
							</a>
							<a href="{{ route('occurrence.edit', ['id'=>$happen['id']]) }}" class="btn-sm btn-success">
								<span data-feather="edit"></span> Edit<span class="sr-only">(current)</span>
							</a>
							<form class="d-inline" onsubmit="return confirm('Permanently Delete This Sky Event?')" action="{{ route('occurrence.destroy', ['id'=>$happen['id']]) }}" method="POST">
								@csrf
								@method('DELETE')
								<button type="submit" class="btn-sm btn-danger" value="Delete" name="submit">
									<span data-feather="trash"></span> Delete<span class="sr-only">(current)</span>
								</button>
							</form>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
		<div class="pagination justify-content-center"> {{ $occurrence->links() }} </div>
	</div>
@endsection