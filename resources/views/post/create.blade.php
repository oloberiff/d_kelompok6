@extends('templates.home')
@section('title')
    Create Post
@endsection
@section('content')
    <div class="container" >
        <h3>Create Post</h3>
        <hr>
        @if (session('status'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{ session('status') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
        @endif
        <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
            <div class="card-header bg-primary text-white">
                <h5> Create a New Post</h5>
            </div>
            <div class="card-body">
                <div class="container text-primary">
                    <form action="{{ route('post.store') }}" class="form-group" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="title">Title</label>
                            </div>
                            <div class="col-md-8">
                                <input value="{{ old('title') }}" type="text" class="form-control {{$errors->first('title') ? "is-invalid": ""}}" name="title" id="title">
                                    <div class="invalid-feedback">
                                        {{$errors->first('title')}}
                                    </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="bodypost">Content</label>
                            </div>
                            <div class="col-md-8">
                                <textarea class="form-control {{$errors->first('bodypost')? "is-invalid": ""}}" name="bodypost" id="bodypost" cols="30" rows="5"></textarea>
                                <div class="invalid-feedback">
                                    {{$errors->first('bodypost')}}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                Picture
                            </div>
                            <div class="col-md-8">
                                <div class="custom-file">
                                    <label for="picture" class="custom-file-label">Picture</label>
                                        <input type="file" class="custom-file-input {{$errors->first('picture') ? "is-invalid": ""}}" name="picture" id="picture">
                                            <div class="invalid-feedback">
                                                {{$errors->first('picture')}}
                                            </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-sm-4">
                                <button type="submit" class="btn btn-outline-primary">Create</button>
                            </div>
                        </div>
                    </form>   
                </div>
            </div>
        </div>
    </div>
@endsection