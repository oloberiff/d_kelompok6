@extends('templates.home')
@section('title')
    Post
@endsection
@section('content')
    <h1>{{ $post['title'] }}</h1>
    <hr>
    <br>
    <div class="card bg-white border-info" style="max-width: 70%; margin: auto; min-height: 400px;">

			<div class="card mb-4">
                <div class="card-body">
                    <img src="{{asset('storage/'.$post['picture'])}}" class="img-thumbnail" width="150px" alt="N/A">
                    <p class="card-text"> {{$post['bodypost']}} </p>
                </div>
                <div class="card-footer text-muted">
                    Posted on {{ $post['created_at'] }}
                </div>
            </div>
    </div>
@endsection