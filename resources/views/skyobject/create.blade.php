@extends('templates.home')
@section('title')
    Create Sky Object Data
@endsection
@section('content')
    <div class="container" >
        <h3>Input Sky Object Data</h3>
        <hr>
        @if (session('status'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{ session('status') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
        @endif
        <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
            <div class="card-header bg-primary text-white">
                <h5> Input Sky Object Data</h5>
            </div>
            <div class="card-body">
                <div class="container text-primary">
                    <form action="{{ route('skyobject.store') }}" class="form-group" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="name" >Sky Object Name</label>
                            </div>
                            <div class="col-md-8">
                                <input value="{{ old('name') }}" type="text" class="form-control {{$errors->first('name') ? "is-invalid": ""}}" name="name" id="name">
                                    <div class="invalid-feedback"> 
                                        {{$errors->first('name')}}
                                    </div>
                            </div>
                        </div>
                        <br>
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="type" >Type</label>
                            </div>
                            <div class="col-md-8">
                                <input value="{{ old('type') }}" type="text" class="form-control {{$errors->first('type') ? "is-invalid": ""}}" name="type" id="type">
                                    <div class="invalid-feedback"> 
                                        {{$errors->first('type')}}
                                    </div>
                            </div>
                        </div>
                        <br>
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="distance">Distance</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control {{$errors->first('distance') ? "is-invalid": ""}}" name="distance" id="distance">
                                    <div class="invalid-feedback">
                                        {{$errors->first('distance')}}
                                    </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                Picture
                            </div>
                            <div class="col-md-8">
                                <div class="custom-file">
                                    <label for="picture" class="custom-file-label">Picture</label>
                                        <input type="file" class="custom-file-input {{$errors->first('picture') ? "is-invalid": ""}}" name="picture" id="picture">
                                            <div class="invalid-feedback">
                                                {{$errors->first('picture')}}
                                            </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-sm-4">
                                <button type="submit" class="btn btn-outline-primary">Create</button>
                            </div>
                        </div>
                    </form>   
                </div>
            </div>
        </div>
    </div>
@endsection