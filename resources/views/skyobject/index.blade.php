@extends('templates.home')
@section('title')
    Sky Object Index 
@endsection
@section('css')
<style>
	body{
		padding-top: 30px;
	}
	th, td {
		padding: 10px;
		text-align: center;
	}
	td a{
		margin: 3px;
		align-content: center;
		color: white;
	}
	td a:hover{
		text-decoration: none;
	}
	td button{
		margin-top: 5px;
		cursor: pointer;
	 }
</style>
@endsection
@section('content')
	<div class="container">
		<h3> Sky Object Index</h3>
		<hr>
		@if (session('status'))
		<div class="alert alert-info alert-dismissible fade show" role="alert">
			<strong> {{ session('status') }} </strong>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif
		<div class="row">
			<div class="col-md-2">
				<a href=" {{ route('skyobject.create') }} " class="btn btn-outline-primary">
					<span data-feather="plus-square"></span> Add Sky Object<span class="sr-only">(current)</span>
				</a>
			</div>
			<div class="col-md-8">
				<form action=" {{ route('skyobject.search') }} " method="get">
					<div class="input-group custom-search-form">
						<input type="text" name="search" placeholder="Filter by Name" class="form-control">
						<span class="input-group-btn">&nbsp;
							<button class="btn btn-outline-dark" type="submit">Filter</button>
						</span>
					</div>
				</form>
			</div>
		</div>
		<br>
		<div class="tabel-responsive">
			<table class="table table-hover">
				<thead>
					<tr class="table-primary">
						<th scope="col">ID</th>
						<th scope="col">Name</th>
                        <th scope="col">Type</th>
						<th scope="col">Picture</th>
						<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
				@foreach ($skyobject as $skyobjects)
					<tr>
						<th>{{ $skyobjects['id'] }}</th>
                        <th>{{ $skyobjects['name'] }}</th>
                        <th>{{ $skyobjects['type'] }}</th>
						<td>
							<img src="{{asset('storage/'.$skyobjects['picture'])}}" class="img-thumbnail" width="80px" alt="">
						</td>
						<td>
							
							<a href="{{ route('skyobject.show', ['id'=>$skyobjects['id']]) }}" class="btn-sm btn-primary">
								<span data-feather="eye"></span> Detail<span class="sr-only">(current)</span>
							</a>
							<a href="{{ route('skyobject.edit', ['id'=>$skyobjects['id']]) }}" class="btn-sm btn-success">
								<span data-feather="edit"></span> Edit<span class="sr-only">(current)</span>
							</a>
							<form class="d-inline" onsubmit="return confirm('Permanently Delete Sky Object?')" action="{{ route('skyobject.destroy', ['id'=>$skyobjects['id']]) }}" method="POST">
								@csrf
								@method('DELETE')
								<button type="submit" class="btn-sm btn-danger" value="Delete" name="submit">
									<span data-feather="trash"></span> Delete<span class="sr-only">(current)</span>
								</button>
							</form>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
		<div class="pagination justify-content-center"> {{ $skyobject->links() }} </div>
	</div>
@endsection