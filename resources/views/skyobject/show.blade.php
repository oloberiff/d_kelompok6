@extends('templates.home')
@section('title')
    Detail Sky Object
@endsection
@section('content')
    <h1>Detail Sky Object </h1>
    <hr>
    <br>
    <div class="card bg-white border-info" style="max-width: 70%; margin: auto; min-height: 400px;">
			<div class="row" style="padding: 25px;">
				<div class="col-md-6 offset-sm-2 offset-md-3 ">
					<img src="{{asset('storage/'.$skyobject['picture'])}}" class="img-thumbnail" width="300px" alt="">
				</div>
            </div>
            
			<div class="row">
				<div class="col-md-12 text-center">
					<h3>{{ $skyobject['name'] }} </h3>
				</div>
			</div>
            <hr>
            
			<br>
			
			<div class="row">
				<div class="col-md-3 offset-md-2 col-sm-3 offset-sm-2">
					<b>Type</b>
				</div>
				<br>
			</div>
				<div class="col-md-5 col-sm-4">
					{{ $skyobject['type'] }}
				</div>

			<div class="row">
				<div class="col-md-3 offset-md-2 col-sm-3 offset-sm-2">
					<b>Distance</b>
				</div>
				<div class="col-md-4 col-sm-4">
					{{ $skyobject['distance'] }}
				</div>
				<br>
			</div>
    </div>
@endsection