@extends('templates.home')
@section('title')
    Detail Spacecraft
@endsection
@section('content')
    <h1>Detail Spacecraft </h1>
    <hr>
    <br>
    <div class="card bg-white border-info" style="max-width: 70%; margin: auto; min-height: 400px;">
			<div class="row" style="padding: 25px;">
				<div class="col-md-6 offset-sm-2 offset-md-3 ">
					<img src="{{asset('storage/'.$spacecraft['picture'])}}" class="img-thumbnail" width="300px" alt="N/A">
				</div>
            </div>
            
			<div class="row">
				<div class="col-md-12 text-center">
					<h3>{{ $spacecraft['name'] }} </h3>
				</div>
			</div>
            <hr>
            
			<br>
			
			<div class="row">
				<div class="col-md-3 offset-md-2 col-sm-3 offset-sm-2">
					<b>Manufacturer</b>
				</div>
				<div class="col-md-4 col-sm-4">
					{{ $spacecraft['manufacturer'] }}
				</div>
				<br>
			</div>

			<div class="row">
				<div class="col-md-3 offset-md-2 col-sm-3 offset-sm-2">
					<b>Launch Date</b>
				</div>
				<div class="col-md-4 col-sm-4">
					{{ $spacecraft['launch_date'] }}
				</div>
				<br>
			</div>

			<div class="row">
				<div class="col-md-3 offset-md-2 col-sm-3 offset-sm-2">
					<b>Description</b>
				</div>
				<div class="col-md-4 col-sm-4">
					{{ $spacecraft['description'] }}
				</div>
				<br>
			</div>
			<br><br>
    </div>
@endsection