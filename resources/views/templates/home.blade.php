<!doctype html>
<html lang="en">
 	<head>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		 <meta name="description" content="">
		 <meta name="author" content="">
		 <meta name="csrf-token" content="{{ csrf_token() }}">
		 <title>@yield('title')</title>
		 <!-- Bootstrap core CSS -->
		 <link href="{{asset('css/app.css')}}" rel="stylesheet">
		 <link href="{{asset('css/dashboard.css')}}" rel="stylesheet">
		 @yield('css')
 	</head>
 	<body>
		<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
				<a class="navbar-brand" href="/"><span data-feather="book-open"></span> INFO ASTRONOMI</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" datatarget="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" arialabel="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarsExampleDefault">
					<ul class="navbar-nav mr-auto">
					</ul>
					<form class="form-inline my-2 my-lg-0">
					<input class="form-control mr-sm-2" type="text" placeholder="Search" arialabel="Search">
					<button class="btn btn-outline-primary my-2 my-sm-0" type="submit"><span data-feather="search"></span> Search</button>
					</form>
				</div>
			</nav>
		<div class="container-fluid">
			<div class="row">
				<nav class="col-md-2 d-none d-md-block bg-light sidebar">
					<div class="sidebar-sticky">
						<ul class="nav flex-column">
							<li class="nav-item ">
								<a class="nav-link active" href="/user">
								<span data-feather="user"></span>
								Users
								</a>
							</li>
							<li class="nav-item ">
								<a class="nav-link active" href="/post">
								<span data-feather="book"></span>
								Post
								</a>
							</li>
							<li class="nav-item ">
								<a class="nav-link active" href="/skyobject">
								<span data-feather="sun"></span>
								Sky Object
								</a>
							</li>
							<li class="nav-item ">
								<a class="nav-link active" href="/spacecraft">
								<span data-feather="send"></span>
								Spacecraft
								</a>
							</li>
							<li class="nav-item ">
								<a class="nav-link active" href="/occurrence">
								<span data-feather="star"></span>
								Sky Events
								</a>
							</li>
						</ul>
					</div>
				</nav>
				<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
				@yield('content')
				</main>
				</div>
			</div>
		<footer class="footer">
			<div class="container text-center text-light">
				<span>&copy;D-16 2017</span>
			</div>
		</footer>
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="{{asset('js/app.js')}}"></script>
		<!-- Icons -->
		<script src="{{asset('js/feather.min.js')}}"></script>
		<script> feather.replace() </script>
		@yield('js')
	</body>
</html>