@extends('templates.home')
@section('title')
    Detail User
@endsection
@section('content')
    <h1>Detail User </h1>
    <hr>
    <br>
    <div class="card bg-white border-info" style="max-width: 70%; margin: auto; min-height: 400px;">
			<div class="row" style="padding: 25px;">
				<div class="col-md-6 offset-sm-2 offset-md-3 ">
					<img src=" {{ asset('storage/'.$user['avatar']) }} " alt="Avatar" style="width:300px; height: 300px;" class="img-thumbnail">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<h3>{{ $user['name'] }} </h3>
				</div>
			</div>
			<hr>
			<br>
			
			<div class="row">
				<div class="col-md-3 offset-md-2 col-sm-3 offset-sm-2">
					<b>Username</b>
				</div>
				<div class="col-md-5 col-sm-4">
					{{ $user['username'] }}
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 offset-md-2 col-sm-3 offset-sm-2">
					<b>Email</b>
				</div>
				<div class="col-md-4 col-sm-4">
					{{ $user['email'] }}
				</div>
				<br>
			</div>

			<div class="row">
				<div class="col-md-3 offset-md-2 col-sm-3 offset-sm-2">
					<b>Status</b>
				</div>
				<div class="col-md-4 col-sm-4">
					@if($user->status == "ACTIVE")
						<span class="badge badge-primary">{{ $user['status'] }}</span>
					@else
						<span class="badge badge-danger">{{ $user['status'] }}</span>
					@endif
				</div>
				<br>
				<br>
			</div>
    </div>
@endsection