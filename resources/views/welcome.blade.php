<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .article {
                text-align: justify;
                padding: 100px;

            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            
            <div class="content">
                <div class="title m-b-md">
                    INFO ASTRONOMI
                </div>

                <div class="links">
                    <a href="/post">BLOG</a>
                    <a href="/spacecraft">SPACECRAFT</a>
                    <a href="/skyobject">SKY OBJECT</a>
                    <a href="/occurrence">SKY EVENT</a>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="article">
                <div class="col-md-8">
                    <h1 class="my-4">INFO ASTRONOMI
                        <small class="text-success">BLOG</small>
                    </h1>
                    @foreach ($post as $posts)
                        <div class="card mb-4">
                            <div class="card-body">
                                <h2 class="card-title"> {{$posts['title']}} </h2>
                                <img src="{{asset('storage/'.$posts['picture'])}}" class="img-thumbnail" width="150px" alt="N/A">
                                <p class="card-text"> {{$posts['bodypost']}} </p>
                            </div>
                            <div class="card-footer text-muted">
                                Posted on {{ $posts['created_at'] }}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </body>
</html>
