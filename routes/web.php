<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');


Route::resource('user', 'UserController');
Route::get('/user', 'UserController@search')->name('user.search');

Route::resource('post', 'PostController');
Route::get('/post', 'PostController@search')->name('post.search');

Route::resource('skyobject', 'SkyObjectController');
Route::get('/skyobject', 'SkyObjectController@search')->name('skyobject.search');

Route::resource('spacecraft', 'SpacecraftController');
Route::get('/spacecraft', 'SpacecraftController@search')->name('spacecraft.search');

Route::resource('occurrence', 'OccurrenceController');
Route::get('/occurrence', 'OccurrenceController@search')->name('occurrence.search');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
